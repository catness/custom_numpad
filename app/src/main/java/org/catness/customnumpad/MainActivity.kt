package org.catness.customnumpad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val numpad: Numpad = findViewById(R.id.numpad)
        val textView: TextView = findViewById(R.id.numpadText)
        numpad.setOnClickListenerWithPoint {
            //Toast.makeText(this, "Click...", Toast.LENGTH_LONG).show()
            numpad.processClick(it)
            textView.text = numpad.numbers.joinToString(separator = "")
        }


    }
}