package org.catness.customnumpad

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import java.lang.Math.abs
import java.lang.Math.min

class Numpad @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    private val LOG_TAG: String = this.javaClass.simpleName

    private var radius = 0.0f   // Radius of the button circle.
    private var startX = 0.0f   // offset of the 1st button
    private var startY = 0.0f

    // 12 buttons inside the numpad
    private var buttons: MutableList<PointF> = mutableListOf()
    // numbers entered from the numpad
    private var _numbers: MutableList<Int> = mutableListOf()
    var numbers: MutableList<Int> = mutableListOf()
        get() = _numbers.toMutableList()

    private var selected = -1  // index of currently clicked button, for highlighting
    var mHandler = Handler(Looper.getMainLooper())  // to reset highlighting

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
        textSize = 70.0f  // can we make it a variable?
        typeface = Typeface.create("", Typeface.BOLD)
    }

    init {
        isClickable = true
    }

    override fun performClick(): Boolean {
        // The call to super.performClick() must happen first,
        // which enables accessibility events as well as calls onClickListener().
        //Log.i(LOG_TAG, "performClick ")
        if (super.performClick()) return true
        invalidate()
        return true
    }

    @ExperimentalStdlibApi
    fun processClick(pointF: PointF) {
        var x = pointF.x
        var y = pointF.y
        // Log.i(LOG_TAG, "clicked $x $y")
        var num = 0
        buttons.forEach {
            // Log.i(LOG_TAG, "trying ${it.x} ${it.y}")
            if ((it.x - x) * (it.x - x) + (it.y - y) * (it.y - y) < radius * radius) {
                // Log.i(LOG_TAG, "Button found: $num")
                with(_numbers) {
                    when (num) {
                        9 -> clear()
                        10 -> add(0)
                        11 -> removeLastOrNull()
                        else -> add(num + 1)
                    }
                }
                // Log.i(LOG_TAG,"numbers= $_numbers")
                selected = num
                mHandler.removeCallbacksAndMessages(null)
                invalidate()
                // reset highlighting after 1 sec
                mHandler.postDelayed({
                    selected = -1
                    invalidate()
                }, 1000)

                return
            }
            num++
        }
    }

    override fun performContextClick(x: Float, y: Float): Boolean {
        // Log.i(LOG_TAG, "performContextClick $x $y")
        if (super.performContextClick(x, y)) return true
        invalidate()
        return true
    }

    // The onSizeChanged() method is called any time the view's size changes,
    // including the first time it is drawn when the layout is inflated.
    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        radius = (min(width, height) / 8.0 * 0.8).toFloat()
        buttons.clear()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.color = Color.LTGRAY
        var offset = 80f  // can we make it a variable?

        var digit = 1
        var btn = 0
        startX = radius + offset
        startY = radius + offset
        var create = buttons.isEmpty()
        for (row in 0..3) {
            var y: Float = row * radius * 2 + startX + radius
            for (col in 0..2) {
                var x: Float = col * radius * 2 + startY + radius
                if(create) buttons.add(PointF(x, y))
                paint.color = Color.DKGRAY
                canvas.drawCircle(x, y, radius, paint)
                paint.color = if (btn == selected) Color.GREEN else Color.LTGRAY
                btn++
                canvas.drawCircle(x, y, radius - 10, paint)
                paint.color = Color.BLACK

                var label = when (row) {
                    3 -> when (col) {
                        0 -> "C"
                        2 -> "<--"
                        else -> digit.toString()
                    }
                    else -> digit.toString()
                }
                canvas.drawText(label, x, y + paint.textSize / 4, paint)
                if (digit > 0 && ++digit == 10) {
                    digit = 0
                }
            }
        }
    }
}

// https://stackoverflow.com/questions/1967039/onclicklistener-x-y-location-of-event
@SuppressLint("ClickableViewAccessibility")
fun View.setOnClickListenerWithPoint(action: (PointF) -> Unit) {
    val coordinates = PointF()
    //val screenPosition = IntArray(2)
    setOnTouchListener { v, event ->
        if (event.action == MotionEvent.ACTION_DOWN) {
            //v.getLocationOnScreen(screenPosition)
            //coordinates.set(event.x.toInt() + screenPosition[0], event.y.toInt() + screenPosition[1])
            coordinates.set(event.x, event.y)
        }
        false
    }
    setOnClickListener {
        action.invoke(coordinates)
        // Log.i("VIEW", "coords = $coordinates")
    }
}

