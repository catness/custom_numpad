# About the project

**Custom Numpad** is an example for [Advanced Android in Kotlin: creating custom views](https://codelabs.developers.google.com/codelabs/advanced-andoid-kotlin-training-custom-views). My custom view is a little more complex than the codelab example: it is a numpad with round buttons, which allows to enter a string of digits. There are also buttons for backspace and clear all. The buttons are highlighted for a short time upon clicking.

It's certainly  possible to do the same thing with each button being a separate widget in a Grid layout or something, but the purpose of the exercise was to make it all in one custom view.

Unfortunately, no accessibility options - I couldn't figure out how to add accessibility feedback separately for each hotspot.

The apk is here: [numpad.apk](apk/numpad.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
